### Document

- [apollo-aws-lambda](https://github.com/apollographql/apollo-server#aws-lambda)
- [serverless graphql apollo](https://github.com/serverless/serverless-graphql-apollo/blob/master/package.json)
- [serverless-framework](https://serverless.com/framework/)

### Notes

- AWS_PROXY

The Lambda proxy integration, designated by AWS_PROXY in the API Gateway REST API, is for integrating a method request with a Lambda function in the backend. With this integration type, API Gateway applies a default mapping template to send the entire request to the Lambda function and transforms the output from the Lambda function to HTTP responses.

### Case study

#### Cloudfront in Slack

- https://www.slideshare.net/AmazonWebServices/secured-api-acceleration-with-engineers-from-amazon-cloudfront-and-slack?sc_channel=EL&sc_campaign=Training_2016_vid&sc_medium=YouTube&sc_content=video518&sc_detail=TRAINING&sc_country=US
- https://www.youtube.com/watch?v=oVaTiRl9-v0
