import { send } from './nodemailer'

const templates = {
  GETTING_EMAIL: send({
    template: `${__dirname}/static/cerberus-responsive.pug`,
    subject: 'Hello from Nodemailer',
  })
}

export default templates
