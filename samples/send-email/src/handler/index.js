// import handler from 'handler'
import middlewares from './middlewares'

exports.handler = function (event, context, callback) {
  middlewares.go(event, context, callback, () => {})
}
