import pug from "pug"
import nodemailer from "nodemailer"
import { email } from './config'

const debug = require('debug')('serverless:nodemailer')

const transporter = nodemailer.createTransport({
  service: email.service,
  auth: email.auth,
  logger: false,
  debug: false // include SMTP traffic in the logs
}, {
  // default message fields
  // sender info
  from: 'Admin from particle4dev.com<admin@particle4dev.com>',
  headers: {
    'X-Laziness-level': 1000 // just an example header, no need to use this
  }
})

export function send ({
  template,
  subject,
}) {

  const compiledFunction = pug.compileFile(template)
  const message = {
    // Subject of the message
    subject,

    // plaintext body
    // text: 'Hello to myself!',

    // HTML body
    html: compiledFunction({
      name: 'Timothy'
    }),

    // An array of attachments
    attachments: [
      // String attachment
      {
        filename: 'notes.txt',
        content: 'Some notes about this e-mail',
        contentType: 'text/plain' // optional, would be detected from the filename
      },

      // Binary Buffer attachment
      {
        filename: 'image.png',
        content: Buffer.from(
          'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
          '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
          'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC',
          'base64'
        ),
        cid: 'note@example.com' // should be as unique as possible
      },
      /*
      // File Stream attachment
      {
        filename: 'nyan cat ✔.gif',
        path: __dirname + '/assets/nyan.gif',
        cid: 'nyan@example.com' // should be as unique as possible
      }
      */
    ]
  }
  return (to, callback) => {
    // Comma separated list of recipients
    message.to = to
    transporter.sendMail(message, (error, info) => {
      if (error) {
        debug(`Error occurred: error.message`)
        callback(error, null)
      } else {
        debug('Message sent successfully!')
        // only needed when using pooled connections
        // transporter.close()
        callback(null, "Success")
      }
    })
  }
}

export default transporter
