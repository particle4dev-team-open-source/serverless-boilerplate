import Middleware from './middleware'
import cerberusResponsive from './cerberus-responsive'

const middleware = new Middleware()
middleware.use(cerberusResponsive)

export default middleware