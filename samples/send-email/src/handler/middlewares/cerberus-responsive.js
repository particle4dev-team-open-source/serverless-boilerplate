import transporter from '../nodemailer'
import templates from '../templates'

const debug = require('debug')('serverless:middlewares:cerberus-responsive')

export default (event, context, callback, next) => {
  const config = JSON.parse(event.Records[0].Sns.Message)
  if(config.type !== 'GETTING_EMAIL') {
    next()
  } else {
    debug(`Send message to ${config.to}`)
    debug(`GETTING_EMAIL ${config.type}`)

    const send = templates[config.type]
    send(config.to, callback)  
  }
}
