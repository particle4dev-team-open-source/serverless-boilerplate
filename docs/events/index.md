
s3        Generates a sample Amazon S3 event
sns       Generates a sample Amazon SNS event
kinesis   Generates a sample Amazon Kinesis event
dynamodb  Generates a sample Amazon DynamoDB event
api       Generates a sample Amazon API Gateway event
schedule  Generates a sample scheduled event
