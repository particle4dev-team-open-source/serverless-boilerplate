const debug = require('debug')('serverless:middlewares:cerberus-responsive')

export default (event, context, callback, next) => {
  debug(`Send message to cerberus-responsive ${JSON.stringify(event, null, 2)}`)
  callback(null, 'finished')
}
