
data "template_file" "bucket_policy" {
  template = "${file("${path.module}/templates/bucket_policy.json")}"

  vars {
    subdomain = "${local.subdomain}"
  }
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${local.subdomain}"
  acl = "public-read"
  policy = "${data.template_file.bucket_policy.rendered}"
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT", "POST"]
    allowed_origins = ["*"]
  }
}

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromS3"
  action        = "lambda:InvokeFunction"
  function_name = "${var.lambda_function_name}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${aws_s3_bucket.s3_bucket.arn}"
}

resource "aws_s3_bucket_notification" "s3_bucket_notification" {
  bucket = "${aws_s3_bucket.s3_bucket.id}"
  lambda_function {
    lambda_function_arn = "${var.lambda_function_arn}"
    events              = ["s3:ObjectCreated:*"]
    # filter_prefix       = "origin/"
  }
}