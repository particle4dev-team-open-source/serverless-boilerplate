resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = "${var.lambda_function_name}"
  principal     = "sns.amazonaws.com"
  source_arn    = "${aws_sns_topic.sns_topic.arn}"
}

resource "aws_sns_topic" "sns_topic" {
  name = "${local.topic}"
}

resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  topic_arn = "${aws_sns_topic.sns_topic.arn}"
  protocol  = "lambda"
  endpoint  = "${var.lambda_function_arn}"
}
