variable "region" {
  description = "The AWS region to create resources in."
  default = "ap-southeast-1"
}

variable "availability_zone" {
  description = "A list of Availability zones in the region"
  default     = [
    "ap-southeast-1a",
    "ap-southeast-1b"
  ]
}